// ConsoleApplication1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <Windows.h>
#include "container.h"

using namespace std;

void In(container *a, ifstream &ifst);
void Out(container *a, ofstream &ofst);
void Outforuser(container *a, ofstream &ofst);
void clear(container *a);

int main(int argc, char *argv[])
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	if (argc != 3)
	{
		cout << "������. ������� �������� ���������� ���������� ��������� ������";
		return 0;
	}

	ifstream in(argv[1]);
	ofstream out(argv[2]);

	if (!in.is_open())
	{
		cout << "������. ���� " << argv[1] << " �� ������ ";
		return 0;
	}

	container *cont = new container;

	In(cont, in);

	Outforuser(cont, out);

	clear(cont);

    return 0;
}

