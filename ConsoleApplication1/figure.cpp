#include "stdafx.h"
#include "figure.h"
#include "ball.h"
#include "paral.h"
#include <fstream>
using namespace std;

void OutBall(ball *a, ofstream &ofst);
void OutParal(paral *a, ofstream &ofst);
ball *InBall(ifstream &ifst);
paral *InParal(ifstream &ifst);

void Out(figure *a,ofstream &ofst)
{
	switch (a->key)
	{
	case BALL:
		ofst << "���\n";
		OutBall((ball*)a->element, ofst);
		break;
	case PARAL:
		ofst << "��������������: \n";
		OutParal((paral*)a->element, ofst);
		break;
	}
}

void In(figure *add, ifstream &ifst)
{
	int key;
	ifst >> key;
	switch (key)
	{
	case 1:
		add->key = BALL;
		add->element = (void*)InBall(ifst);
		break;
	case 2:
		add->key = PARAL;
		add->element = (void*)InParal(ifst);
		break;
	}
}